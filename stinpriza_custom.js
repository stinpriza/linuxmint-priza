// This file can be used to configure global preferences for Firefox
// Example: Homepage
//pref("browser.startup.homepage", "https://search.stinpriza.org");

// priza customisations for firefox, privacy
user_pref("set browser.urlbar.trimURLs","false");
user_pref("browser.search.countryCode", "GR");
user_pref("browser.search.region", "GR");
user_pref("browser.search.defaultenginename", "searx");
user_pref("browser.search.defaultengineloadPath", "[https]search.stinpriza.org/searx.xml");
user_pref("browser.search.defaultengineorigin", "verified");
user_pref("browser.search.selectedEngine", "searx");
user_pref("datareporting.policy.dataSubmissionEnabled", "false");
user_pref("datareporting.healthreport.service.enabled", "false");
user_pref("datareporting.healthreport.uploadEnabled", "false");
user_pref("toolkit.telemetry.archive.enabled", "false");
user_pref("toolkit.telemetry.enabled", "false");
user_pref("toolkit.telemetry.rejected", "true");
user_pref("toolkit.telemetry.server", "");
user_pref("toolkit.telemetry.unified", "false");
user_pref("toolkit.telemetry.unifiedIsOptIn", "false");
user_pref("toolkit.telemetry.prompted", "2");
user_pref("toolkit.telemetry.rejected", "true");

