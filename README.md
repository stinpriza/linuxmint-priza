# linuxmint-priza, scripts licensed under GPLv3

DESCRIPTION 

customize linuxmint 20 mate edition, for Stin Priza customers.
be careful! not to be used in general.

script to be run as regular user (not root!!!). eg:


INSTALL

Linux Mint 20 "Ulyana" : 

$ wget https://framagit.org/stinpriza/linuxmint-priza/raw/master/linuxmint.sh

( customize/edit/review if needed, then run)

$ sh linuxmint.sh

you'll be asked for sudo pass (needed for system updates, etc). 
reboot the machine after finishing and check functionality.
