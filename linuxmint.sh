#!/bin/sh

MYUSER=`whoami`
    if [ "${MYUSER}" = "root" ]; then
        echo "[X] This script should not be executed as root!! run as user without sudo!!"
    fi

# keyboard layout & screensaver fixes
echo 'keyboard layout & screensaver settings...'
dconf write /org/mate/desktop/peripherals/keyboard/kbd/layouts "['us', 'gr']"
dconf write /org/mate/desktop/peripherals/keyboard/kbd/options "['terminate\tterminate:ctrl_alt_bksp', 'currencysign\teurosign:e', 'grp\tgrp:alt_shift_toggle']"
dconf write /org/mate/desktop/session/idle-delay "60"
dconf write /org/mate/power-manager/sleep-display-ac "0"
gsettings set org.mate.screensaver lock-enabled false
gsettings set org.mate.caja.desktop trash-icon-visible true
# add GR mirrors for faster updates
sudo rm -f /etc/apt/sources.list.d/*
sudo wget -O /etc/apt/sources.list.d/official-package-repositories.list https://framagit.org/stinpriza/linuxmint-priza/raw/master/official-package-repositories.list

# system upgrades
sudo apt-get update
tput setaf 2; echo "repositories updated! removing unecessary packages, installing new ones, and upgrading system now..."
# remove unnecessary packages
sudo apt-get purge -y blueberry bluetooth bluez-cups bluez-obexd brltty* espeak* espeak-data* gir1.2-gnomebluetooth-1.0 gnome-bluetooth gnome-orca libespeak1 libgnome-bluetooth13 pulseaudio-module-bluetooth speech-dispatcher*
# remove asian fonts
sudo apt-get purge -y fonts-deva-extra fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lohit-beng-assamese fonts-lohit-beng-bengali fonts-lohit-deva fonts-lohit-gujr fonts-lohit-guru fonts-lohit-knda fonts-lohit-mlym fonts-lohit-orya fonts-lohit-taml fonts-lohit-taml-classical fonts-lohit-telu fonts-nanum fonts-orya-extra fonts-pagul fonts-smc fonts-telu-extra fonts-tlwg-garuda fonts-tlwg-kinnari fonts-tlwg-laksaman fonts-tlwg-loma fonts-tlwg-mono fonts-tlwg-norasi fonts-tlwg-typist fonts-tlwg-typo fonts-tlwg-umpush fonts-tlwg-waree fonts-wqy-microhei 
# greek & system packages install
sudo apt-get install -y hunspell-el language-pack-el firefox-locale-el language-pack-gnome-el thunderbird-locale-el gimp-help-el language-pack-el-base psensor libreoffice-l10n-el fonts-inconsolata xfonts-terminus-oblique xfonts-terminus libreoffice-help-el deborphan bash-completion pidgin pidgin-otr pidgin-plugin-pack mate-tweak inkscape dconf-editor keepassxc atril caja-extensions-common caja-rename gcp p7zip-rar zsh unrar
# multimedia codecs install
sudo apt-get install -y gstreamer1.0-plugins-ugly gstreamer1.0-vaapi libdvdcss2 unshield cabextract vlc ffmpeg youtube-dl
# update mintupdate first
sudo apt-get -y install mintupdate
# system upgrades
sudo apt-get update && sudo apt-get -y upgrade
sudo apt-get -y dist-upgrade
echo 'system upgraded! continuing...'

# package cleanup
echo 'done, cleaning up...'
sudo apt-get -f install
sudo apt-get purge -y `deborphan`
sudo apt-get purge -y `deborphan`
sudo apt-get purge -y `deborphan`
sudo apt-get autoremove -y
sudo apt-get clean
echo 'basic installation, settings and cleaning up is done!'

# greek system-wide
echo 'updating system locales, adding greek as default lang...'
sudo rm -f ~/locale.gen* /etc/locale.gen
sudo wget -O /etc/locale.gen https://framagit.org/stinpriza/linuxmint-priza/raw/master/locale.gen
sudo wget -O /var/lib/locales/supported.d/el https://framagit.org/stinpriza/linuxmint-priza/raw/master/el
sudo wget -O /var/lib/locales/supported.d/en https://framagit.org/stinpriza/linuxmint-priza/raw/master/en
sudo dpkg-reconfigure --frontend=noninteractive locales
sudo update-locale LANG=el_GR.UTF-8


# fix mint-update
echo 'adjusting update manager to check for new packages 10mins after boot and then weekly...'
gsettings set com.linuxmint.updates autorefresh-days 7
gsettings set com.linuxmint.updates autorefresh-hours 0
gsettings set com.linuxmint.updates autorefresh-minutes 0
gsettings set com.linuxmint.updates refresh-days 0
gsettings set com.linuxmint.updates refresh-hours 0
gsettings set com.linuxmint.updates refresh-minutes 10
gsettings set com.linuxmint.updates window-width 790
gsettings set com.linuxmint.updates window-height 693
gsettings set com.linuxmint.updates hide-systray true
gsettings set com.linuxmint.updates hide-window-after-update true
gsettings set com.linuxmint.updates show-welcome-page false


# weather
echo 'adjusting settings needed for panel weather app...'
dconf write /org/mate/panel/objects/clock/prefs/format "'24-hour'"
dconf write /org/mate/panel/objects/clock/prefs/cities "['<location name="" city="Athens" timezone="Europe/Athens" latitude="37.933334" longitude="23.933332" code="LGAV" current="true"/>']"
dconf write /org/mate/panel/objects/clock/prefs/custom-format "''"
dconf write /org/mate/panel/objects/clock/prefs/speed-unit "'Beaufort scale'"

# mozilla adjustments
echo 'removing any open profiles, adding custom adjustments for mozilla products'
rm -rf ~/.mozilla
sudo wget -O /etc/firefox/syspref.js https://framagit.org/stinpriza/linuxmint-priza/raw/master/stinpriza_custom.js

# install torbrowser in /opt
temp="$(curl -s https://www.torproject.org/download/languages/)"; temp2=`echo "${temp}" | grep -E -o '[A-Za-z0-9/_.-]+_en-US.tar.xz' | tail -n 1`; wget -O tor-browser-linux64.tar.xz "https://www.torproject.org$temp2"; tar xvfJ tor-browser-linux64.tar.xz; rm tor-browser-linux64.tar.xz
sudo mv tor-browser_en-US /opt/tor-browser_en-US
#ln -s /opt/tor-browser_en-US/start-tor-browser.desktop ~/Επιφάνεια\ εργασίας/start-tor-browser.desktop
wget -O ~/Επιφάνεια\ εργασίας/Tor_Browser.desktop https://framagit.org/stinpriza/linuxmint-priza/raw/master/Tor_Browser.desktop
chmod +x ~/Επιφάνεια\ εργασίας/Tor_Browser.desktop
echo 'μπορείτε τώρα να ξεκινήσετε τον Tor Browser από την Επιφάνεια εργασίας!'

tput setaf 1; echo "τελειώσαμε, πάμε για reboot!"

tput sgr0

exit 0
